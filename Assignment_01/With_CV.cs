﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;


namespace Assignment_01
{
    //This class calculate all the outputs we want
    public class With_CV
    {
        //instan. simulator calss
        Simulator sim = new Simulator();
        BS_delta delta = new BS_delta();
        Stopwatch watch = new Stopwatch();

        public void scheduler()
        {
            //If users choose to use antithetic, then the value of IO.Antithetic is true, system generates antithetic prices, otherwise system prices option using the original way.
            if (IO.Antithetic == true)
            {
                watch.Start();
                //instan. a random matrix of the size
                double[,] r = new double[IO.Trials, IO.Steps];
                //instan. a matrix to recieve the values of sum_ct, sum_ct2, and option price
                double[,] pricing_matrix = new double[1, 3];
                //compute and generate this random matrix
                r = sim.random();
                //As we need to calculate SE, option price of each simulation is needed, so we assign values to a matrix
                pricing_matrix = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);

                //calculate price, Greek values and SE, pricing_matrix[0,0] is sum_ct, pricing_matrix[0,1] is sum_ct2, pricing_matrix[0,2] is the option price we want.
                IO.O_price = pricing_matrix[0, 2];
                Program.increase(1);
                IO.O_se = SE_ant(pricing_matrix);
                Program.increase(1);
                IO.O_delta = Delta_ant(r);
                Program.increase(1);
                IO.O_gamma = Gamma_ant(r);
                Program.increase(1);
                IO.O_vega = Vega_ant(r);
                Program.increase(1);
                IO.O_theta = Theta_ant(r);
                Program.increase(1);
                IO.O_rho = Rho_ant(r);
                Program.increase(1);
                watch.Stop();
                IO.Timer = watch.Elapsed.Hours.ToString() + ":"
                    + watch.Elapsed.Minutes.ToString() + ":"
                    + watch.Elapsed.Seconds.ToString() + ":"
                    + watch.Elapsed.Milliseconds.ToString();
                watch.Reset();
                Program.finished();
            }
            else if (IO.Antithetic == false)
            {
                watch.Start();
                //instan. a random matrix of the size
                double[,] r = new double[IO.Trials, IO.Steps];
                //instan. a matrix to recieve the values of sum_ct, sum_ct2, and option price
                double[,] pricing_matrix = new double[1, 3];
                //compute and generate this random matrix
                r = sim.random();
                //As we need to calculate SE, option price of each simulation is needed, so we assign values to a matrix
                pricing_matrix = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);

                //calculate price, Greek values and SE, pricing_matrix[0,0] is sum_ct, pricing_matrix[0,1] is sum_ct2, pricing_matrix[0,2] is the option price we want.
                IO.O_price = pricing_matrix[0, 2];
                Program.increase(1);
                IO.O_se = SE(pricing_matrix);
                Program.increase(1);
                IO.O_delta = Delta(r);
                Program.increase(1);
                IO.O_gamma = Gamma(r);
                Program.increase(1);
                IO.O_vega = Vega(r);
                Program.increase(1);
                IO.O_theta = Theta(r);
                Program.increase(1);
                IO.O_rho = Rho(r);
                Program.increase(1);
                watch.Stop();
                IO.Timer = watch.Elapsed.Hours.ToString() + ":"
                    + watch.Elapsed.Minutes.ToString() + ":"
                    + watch.Elapsed.Seconds.ToString() + ":"
                    + watch.Elapsed.Milliseconds.ToString();
                watch.Reset();
                Program.finished();
            }

            //Call Garbage Collector to release RAM
            GC.Collect();
        }


        //==============================================================================================================================================
        //Pricing W/ Antithetic
        //==============================================================================================================================================
        //compute price, the output of this method is a 1X(1+IO.Trials) matrix. The 1st column is option price, the rest are prices of each simulation
        public double[,] Price_ant(double[,] r, int Trials, int Steps, double Tenor, double S, double K, double R, double vol)
        {
            if (IO.Call == true)//call option
            {
                double[,] price = new double[2 * Trials, Steps + 1];//for each trial, two antithetic paths are generated at the same time
                double[,] CT = new double[Trials, 1];
                double dt = Tenor / Convert.ToDouble(Steps);
                double[,] cv = new double[2 * Trials, 1];//each path has a cv
                double delta_1, delta_2;
                double cont_t;
                double erddt = Math.Exp(R * dt);
                for (int a = 0; a <= (2 * Trials) - 1; a++)
                {
                    price[a, 0] = S;
                }

                //each CT needs two cvs, say user input 10000 of trials, then we actually generate 20000 paths, and get 20000 cvs, and then we can get 100000 CTs.
                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>

                {
                    //set the cv of each path to 0
                    cv[a, 0] = 0;
                    cv[(2 * Trials) - 1 - a, 0] = 0;
                    for (int b = 1; b <= Steps; b++)
                    {

                        price[a, b] = price[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                        price[(2 * Trials) - 1 - a, b] = price[(2 * Trials) - 1 - a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) - (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                });
                //for each step, calculate delta
                for (int a = 0; a <= Trials - 1; a++)
                {
                    for (int b = 1; b <= Steps; b++)
                    {
                        cont_t = (b - 1) * dt;
                        delta_1 = BS_delta.Delta(price[a, b - 1], cont_t, Tenor, K, vol, R);

                        delta_2 = BS_delta.Delta(price[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                        //calculate cv
                        cv[a, 0] += delta_1 * (price[a, b] - (price[a, b - 1] * erddt));
                        cv[(2 * Trials) - 1 - a, 0] += delta_2 * (price[(2 * Trials) - 1 - a, b] - (price[(2 * Trials) - 1 - a, b - 1] * erddt));
                    }

                }

                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                {
                    CT[a, 0] = 0.5 * (Math.Max(price[a, Steps] - K, 0) - cv[a, 0] + Math.Max(price[(2 * Trials) - 1 - a, Steps] - K, 0) - cv[(2 * Trials) - 1 - a, 0]);
                });

            

                //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                double[,] sum_ct = new double[1, 3];
                sum_ct[0, 0] = 0;
                sum_ct[0, 1] = 0;
                for (int a = 0; a <= Trials - 1; a++)
                {
                    sum_ct[0, 0] += CT[a, 0];
                    sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                }
                sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                return sum_ct;
            }
            else//put option
            {
                double[,] price = new double[2 * Trials, Steps + 1];//for each trial, two antithetic paths are generated at the same time
                double[,] CT = new double[Trials, 1];
                double dt = Tenor / Convert.ToDouble(Steps);
                double[,] cv = new double[2 * Trials, 1];//each path has a cv
                double delta_1, delta_2;
                double cont_t;
                double erddt = Math.Exp(R * dt);
                for (int a = 0; a <= (2 * Trials) - 1; a++)
                {
                    price[a, 0] = S;
                }

                //each CT needs two cvs, say user input 10000 of trials, then we actually generate 20000 paths, and get 20000 cvs, and then we can get 100000 CTs.
                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                {
                    //set the cv of each path to 0
                    cv[a, 0] = 0;
                    cv[(2 * Trials) - 1 - a, 0] = 0;
                    for (int b = 1; b <= Steps; b++)
                    {

                        price[a, b] = price[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                        price[(2 * Trials) - 1 - a, b] = price[(2 * Trials) - 1 - a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) - (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                });
                //for each step, calculate delta
                for (int a = 0; a <= Trials - 1; a++)
                {
                    for (int b = 1; b <= Steps; b++)
                    {
                        cont_t = (b - 1) * dt;
                        delta_1 = BS_delta.Delta(price[a, b - 1], cont_t, Tenor, K, vol, R);

                        delta_2 = BS_delta.Delta(price[(2 * Trials) - 1 - a, b - 1], cont_t, Tenor, K, vol, R);
                        //calculate cv
                        cv[a, 0] += delta_1 * (price[a, b] - (price[a, b - 1] * erddt));
                        cv[(2 * Trials) - 1 - a, 0] += delta_2 * (price[(2 * Trials) - 1 - a, b] - (price[(2 * Trials) - 1 - a, b - 1] * erddt));
                    }

                }

                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                {
                    CT[a, 0] = 0.5 * (Math.Max(K - price[a, Steps], 0) - cv[a, 0] + Math.Max(K - price[(2 * Trials) - 1 - a, Steps], 0) - cv[(2 * Trials) - 1 - a, 0]);
                });



                //sum all CTs together and get the matrix we want. sum_ct[0,0] is sum_ct, sum_ct[0,1] is sum_ct2, and sum_ct[0,2] is option price.
                double[,] sum_ct = new double[1, 3];
                sum_ct[0, 0] = 0;
                sum_ct[0, 1] = 0;
                for (int a = 0; a <= Trials - 1; a++)
                {
                    sum_ct[0, 0] += CT[a, 0];
                    sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                }
                sum_ct[0, 2] = (sum_ct[0, 0] * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                return sum_ct;
            }

        }

        //The following part calculates Greek values and SE if user choose to use antithetic
        //The pricing_matrix is used to save the output of Price method, and we just need the 3rd lattice of each matrix to calculate Greek values.
        public double Delta_ant(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, 3];
            double[,] pricing_matrix_2 = new double[1, 3];
            pricing_matrix_1 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying + (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying - (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = (pricing_matrix_1[0, 2] - pricing_matrix_2[0, 2]) / (0.002 * IO.Underlying);
            return result;
        }

        public double Gamma_ant(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, 3];
            double[,] pricing_matrix_2 = new double[1, 3];
            double[,] pricing_matrix_3 = new double[1, 3];
            pricing_matrix_1 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying + (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_3 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying - (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = (pricing_matrix_1[0, 2] - (2 * pricing_matrix_2[0, 2]) + pricing_matrix_3[0, 2]) / Math.Pow(0.001 * IO.Underlying, 2);
            return result;
        }

        public double Vega_ant(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, 3];
            double[,] pricing_matrix_2 = new double[1, 3];
            pricing_matrix_1 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility + (0.001 * IO.Volatility));
            pricing_matrix_2 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility - (0.001 * IO.Volatility));
            double result = (pricing_matrix_1[0, 2] - pricing_matrix_2[0, 2]) / (0.002 * IO.Volatility);
            return result;
        }

        public double Theta_ant(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, 3];
            double[,] pricing_matrix_2 = new double[1, 3];
            pricing_matrix_1 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor + (IO.Tenor / Convert.ToDouble(IO.Steps)), IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = -(pricing_matrix_1[0, 2] - pricing_matrix_2[0, 2]) / (IO.Tenor / Convert.ToDouble(IO.Steps));
            return result;
        }

        public double Rho_ant(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, 3];
            double[,] pricing_matrix_2 = new double[1, 3];
            pricing_matrix_1 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate + (0.001 * IO.Risk_free_rate), IO.Volatility);
            pricing_matrix_2 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate - (0.001 * IO.Risk_free_rate), IO.Volatility);
            double result = (pricing_matrix_1[0, 2] - pricing_matrix_2[0, 2]) / (0.002 * IO.Risk_free_rate);
            return result;
        }

        //Calculate standard error, the input of this method is already calculated when calculating option price. Again, pricing_matrix[0,0] is sum_ct,
        //pricing_matirx[0,1] is sum_ct2
        public double SE_ant(double[,] pricing_matrix)
        {
            double SD = Math.Sqrt((pricing_matrix[0, 1] - (pricing_matrix[0, 0] * pricing_matrix[0, 0] / IO.Trials)) * Math.Exp(-2 * IO.Risk_free_rate * IO.Tenor) / (IO.Trials - 1));
            double SE = SD / Math.Sqrt(IO.Trials);
            return SE;
        }
        //==============================================================================================================================================
        //Pricing W/ Antithetic
        //==============================================================================================================================================




        //==============================================================================================================================================
        //Pricing W/O Antithetic
        //==============================================================================================================================================
        public double[,] Price(double[,] r, int Trials, int Steps, double Tenor, double S, double K, double R, double vol)
        {
            if (IO.Call == true)//call option
            {
                double[,] price = new double[Trials, Steps + 1];
                double[,] CT = new double[Trials, 1];
                double dt = Tenor / Convert.ToDouble(Steps);
                double[,] cv = new double[Trials, 1];
                double delta;
                double cont_t;
                double erddt = Math.Exp(R * dt);

                for (int a = 0; a <= Trials - 1; a++)
                {
                    price[a, 0] = S;
                }
                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                {
                    cv[a, 0] = 0;
                    for (int b = 1; b <= Steps; b++)
                    {
                        price[a, b] = price[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                });

                for (int a = 0; a <= Trials - 1; a++)
                {
                   
                    for (int b=1;b<=Steps;b++)
                    {
                        cont_t = (b - 1) * dt;
                        delta = BS_delta.Delta(price[a, b - 1], cont_t, Tenor, K, vol, R);
                        cv[a, 0] += delta * (price[a, b] - (price[a, b - 1] * erddt));
                    }
                    CT[a, 0] = Math.Max(price[a, Steps] - K, 0) - cv[a, 0];
                }

                double[,] sum_ct = new double[1, 3];
                sum_ct[0, 0] = 0;
                sum_ct[0, 1] = 0;
                for (int a = 0; a <= Trials - 1; a++)
                {
                    sum_ct[0, 0] += CT[a, 0];
                    sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                }

                sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);

                return sum_ct;
            }
            else//put option
            {
                double[,] price = new double[Trials, Steps + 1];
                double[,] CT = new double[Trials, 1];
                double dt = Tenor / Convert.ToDouble(Steps);
                double[,] cv = new double[Trials, 1];
                double delta;
                double cont_t;
                double erddt = Math.Exp(R * dt);

                for (int a = 0; a <= Trials - 1; a++)
                {
                    price[a, 0] = S;
                }
                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                {
                    cv[a, 0] = 0;
                    for (int b = 1; b <= Steps; b++)
                    {
                        price[a, b] = price[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                });
                for (int a = 0; a <= Trials - 1; a++)
                {

                    for (int b = 1; b <= Steps; b++)
                    {
                        cont_t = (b - 1) * dt;
                        delta = BS_delta.Delta(price[a, b - 1], cont_t, Tenor, K, vol, R);
                        cv[a, 0] += delta * (price[a, b] - (price[a, b - 1] * erddt));
                    }
                    CT[a, 0] = Math.Max(K - price[a, Steps], 0) - cv[a, 0];
                }
                double[,] sum_ct = new double[1, 3];
                sum_ct[0, 0] = 0;
                sum_ct[0, 1] = 0;
                for (int a = 0; a <= Trials - 1; a++)
                {
                    sum_ct[0, 0] += CT[a, 0];
                    sum_ct[0, 1] += CT[a, 0] * CT[a, 0];
                }
                sum_ct[0, 2] = sum_ct[0, 0] * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                return sum_ct;
            }
        }

        public double Delta(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, 3];
            double[,] pricing_matrix_2 = new double[1, 3];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying + (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying - (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = (pricing_matrix_1[0, 2] - pricing_matrix_2[0, 2]) / (0.002 * IO.Underlying);
            return result;
        }

        public double Gamma(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, 3];
            double[,] pricing_matrix_2 = new double[1, 3];
            double[,] pricing_matrix_3 = new double[1, 3];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying + (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_3 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying - (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = (pricing_matrix_1[0, 2] - (2 * pricing_matrix_2[0, 2]) + pricing_matrix_3[0, 2]) / Math.Pow(0.001 * IO.Underlying, 2);
            return result;
        }

        public double Vega(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, 3];
            double[,] pricing_matrix_2 = new double[1, 3];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility + (0.001 * IO.Volatility));
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility - (0.001 * IO.Volatility));
            double result = (pricing_matrix_1[0, 2] - pricing_matrix_2[0, 2]) / (0.002 * IO.Volatility);
            return result;
        }

        public double Theta(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, 3];
            double[,] pricing_matrix_2 = new double[1, 3];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor + (IO.Tenor / Convert.ToDouble(IO.Steps)), IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = -(pricing_matrix_1[0, 2] - pricing_matrix_2[0, 2]) / (IO.Tenor / Convert.ToDouble(IO.Steps));
            return result;
        }

        public double Rho(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, 3];
            double[,] pricing_matrix_2 = new double[1, 3];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate + (0.001 * IO.Risk_free_rate), IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate - (0.001 * IO.Risk_free_rate), IO.Volatility);
            double result = (pricing_matrix_1[0, 2] - pricing_matrix_2[0, 2]) / (0.002 * IO.Risk_free_rate);
            return result;
        }

        //Calculate standard error, the input of this method is already calculated when calculating option price.
        public double SE(double[,] pricing_matrix)
        {
            double SD = Math.Sqrt((pricing_matrix[0, 1] - (pricing_matrix[0, 0] * pricing_matrix[0, 0] / IO.Trials)) * Math.Exp(-2 * IO.Risk_free_rate * IO.Tenor) / (IO.Trials - 1));
            double SE = SD / Math.Sqrt(IO.Trials);
            return SE;
        }

        //==============================================================================================================================================
        //Pricing W/O Antithetic
        //==============================================================================================================================================
    }
}
