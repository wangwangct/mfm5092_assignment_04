﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_01
{
    public static class IO
    {
        //Input private variable
        private static int trials;
        private static int steps;
        private static double tenor;
        private static bool call,antithetic,cv;
        private static double S, K, r, vol;
        private static int cpu_cores = 1;
        private static int thread = 1;

        //Output
        private static int o_trials;
        private static int o_steps;
        private static double o_average;
        private static double o_price;
        private static double o_delta;
        private static double o_gamma;
        private static double o_vega;
        private static double o_theta;
        private static double o_rho;
        private static double o_se;
        private static string timer="";


        //Auto generated 

        public static int Trials { get => trials; set => trials = value; }
        public static int Steps { get => steps; set => steps = value; }
        public static double Tenor { get => tenor; set => tenor = value; }
        public static bool Call { get => call; set => call = value; }
        public static bool Antithetic { get => antithetic; set => antithetic = value; }
        public static bool CV { get => cv; set => cv = value; }
        public static double Underlying { get => S; set => S = value; }
        public static double Strike_Price { get => K; set => K = value; }
        public static double Risk_free_rate { get => r; set => r = value; }
        public static double Volatility { get => vol; set => vol = value; }
        public static int Cpu_cores { get => cpu_cores; set => cpu_cores = value; }
        public static int Thread { get => thread; set => thread = value; }
        public static int O_trials { get => o_trials; set => o_trials = value; }
        public static int O_steps { get => o_steps; set => o_steps = value; }
        public static double O_price { get => o_price; set => o_price = value; }
        public static double O_average { get => o_average; set => o_average = value; }
        public static double O_delta { get => o_delta; set => o_delta = value; }
        public static double O_gamma { get => o_gamma; set => o_gamma = value; }
        public static double O_vega { get => o_vega; set => o_vega = value; }
        public static double O_theta { get => o_theta; set => o_theta = value; }
        public static double O_rho { get => o_rho; set => o_rho = value; }
        public static double O_se { get => o_se; set => o_se = value; }
        public static string Timer { get => timer; set => timer = value; }
    }
}
