﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Assignment_01
{
    public partial class Form1 : Form
    {
        //input check flags
        bool flag_trials = false;
        bool flag_steps = false;
        bool flag_tenor = false;
        bool flag_S = false;
        bool flag_K = false;
        bool flag_r = false;
        bool flag_vol = false;



        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            IO.Cpu_cores = System.Environment.ProcessorCount;
            label_cores.Text = "Cores:" + Convert.ToString(IO.Cpu_cores);
            progressBar1.Value = 0;
            //textBox_trials.Text = "";
            //textBox_steps.Text = "";
            //textBox_tenor.Text = "";
            //textBox_S.Text = "";
            //textBox_K.Text = "";
            //textBox_r.Text = "";
            //textBox_vol.Text = "";
            //textBox_price.Text = "";
            //textBox_delta.Text = "";
            //textBox_gamma.Text = "";
            //textBox_vega.Text = "";
            //textBox_theta.Text = "";
            //textBox_rho.Text = "";
            //textBox_se.Text = "";
            //textBox_time.Text = "";

        }

        private void button_reset_Click(object sender, EventArgs e)
        {
            //Form1_Load(sender, e);
            IO.Cpu_cores = System.Environment.ProcessorCount;
            label_cores.Text = "Cores:" + Convert.ToString(IO.Cpu_cores);
            progressBar1.Value = 0;
            textBox_trials.Text = "";
            textBox_steps.Text = "";
            textBox_tenor.Text = "";
            textBox_S.Text = "";
            textBox_K.Text = "";
            textBox_r.Text = "";
            textBox_vol.Text = "";
            textBox_price.Text = "";
            textBox_delta.Text = "";
            textBox_gamma.Text = "";
            textBox_vega.Text = "";
            textBox_theta.Text = "";
            textBox_rho.Text = "";
            textBox_se.Text = "";
            textBox_time.Text = "";
        }

        public void finish()
        {
            string price = Convert.ToString(IO.O_price);
            string delta = Convert.ToString(IO.O_delta);
            string gamma = Convert.ToString(IO.O_gamma);
            string vega = Convert.ToString(IO.O_vega);
            string theta = Convert.ToString(IO.O_theta);
            string rho = Convert.ToString(IO.O_rho);
            string se = Convert.ToString(IO.O_se);
            textBox_price.Text = price;
            textBox_delta.Text = delta;
            textBox_gamma.Text = gamma;
            textBox_vega.Text = vega;
            textBox_theta.Text = theta;
            textBox_rho.Text = rho;
            textBox_se.Text = se;
            textBox_time.Text = IO.Timer;

        }
        private void button_go_Click(object sender, EventArgs e)
        {
            //Instan. Option class
            Without_CV opt1 = new Without_CV();
            With_CV opt2 = new With_CV();


            //textBox_time.Text = "00:00:00:00";
            //watch.Reset();
           // watch.Start();
            progressBar1.Value = 0;
            //when button_go is clicked, check inputs
            textBox_trials_Leave(sender, e);
            textBox_steps_Leave(sender, e);
            textBox_tenor_Leave(sender, e);
            textBox_S_Leave(sender, e);
            textBox_K_Leave(sender, e);
            textBox_r_Leave(sender, e);
            textBox_vol_Leave(sender, e);

          
            if (flag_trials && flag_steps && flag_tenor && flag_S && flag_K && flag_r && flag_vol)
            {
                //Transfer inputs to IO class
                IO.Steps = Convert.ToInt32(textBox_steps.Text);
                IO.Trials = Convert.ToInt32(textBox_trials.Text);
                IO.Tenor = Convert.ToDouble(textBox_tenor.Text);
                IO.Underlying = Convert.ToDouble(textBox_S.Text);
                IO.Strike_Price = Convert.ToDouble(textBox_K.Text);
                IO.Risk_free_rate = Convert.ToDouble(textBox_r.Text);
                IO.Volatility = Convert.ToDouble(textBox_vol.Text);
                if (radioButton_call.Checked)
                {
                    IO.Call = true;
                }
                else if (radioButton_put.Checked)
                {
                    IO.Call = false;
                }

                if (checkBox_vc.Checked)
                {
                    IO.CV = true;
                }
                else
                {
                    IO.CV = false;
                }

                if (checkBox_antithetic.Checked)
                {
                    IO.Antithetic = true;
                }
                else
                {
                    IO.Antithetic = false;
                }
                if(checkBox_MT.Checked)
                {
                    IO.Thread = IO.Cpu_cores;
                }
                else
                {
                    IO.Thread = 1;
                }
                //call scheduler to do calculation, if IO.CV is true, use control variate
                if (IO.CV == false)
                {
                    Thread C = new Thread(new ThreadStart(opt1.scheduler));
                    C.Start();
                }
                else
                {
                    Thread C = new Thread(new ThreadStart(opt2.scheduler));
                    C.Start();
                }                
            }
            else
            {
                MessageBox.Show("Please check your inputs");
            }
        
        }










        //====================The following part is error checking of each textbox==========================
        private void textBox_trials_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_trials.Text, out check))
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_trials.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_trials.Text) < 0)
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_trials.Text = "Wrong";
            }
            //check if it is an integer
            else if (Convert.ToDouble(textBox_trials.Text) % 1 != 0)
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_trials.Text = "Wrong";
            }
            else//if all true, then flag goes to true
            {
                flag_trials = true;
            }
        }

        private void textBox_steps_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number of not
            if (!double.TryParse(textBox_steps.Text, out check))
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_steps.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_steps.Text) < 0)
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_steps.Text = "Wrong";
            }
            //check if it is an integer
            else if (Convert.ToDouble(textBox_steps.Text) % 1 != 0)
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_steps.Text = "Wrong";
            }
            else//if all true, then flag goes to true
            {
                flag_steps = true;
            }
        }

        private void textBox_tenor_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_tenor.Text, out check))
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_tenor.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_tenor.Text) < 0)
            {
                MessageBox.Show("Please enter an integer >0");
                textBox_tenor.Text = "Wrong";
            }
            else//if all true, flag goes to true
            {
                flag_tenor = true;
            }
        }

        private void textBox_S_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_S.Text, out check))
            {
                MessageBox.Show("Please enter a number >0");
                textBox_S.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_S.Text) < 0)
            {
                MessageBox.Show("Please enter a number >0");
                textBox_S.Text = "Wrong";
            }
            else//if all true, flag goes to true
            {
                flag_S = true;
            }
        }

        private void textBox_K_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_K.Text, out check))
            {
                MessageBox.Show("Please enter a number >0");
                textBox_K.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_K.Text) < 0)
            {
                MessageBox.Show("Please enter a number >0");
                textBox_K.Text = "Wrong";
            }
            else//if all true, flag goes to true
            {
                flag_K = true;
            }
        }

        private void textBox_r_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_r.Text, out check))
            {
                MessageBox.Show("Please enter a number >0");
                textBox_r.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_r.Text) < 0)
            {
                MessageBox.Show("Please enter a number >0");
                textBox_r.Text = "Wrong";
            }
            else//if all true, flag goes to true
            {
                flag_r = true;
            }
        }

        private void textBox_vol_Leave(object sender, EventArgs e)
        {
            double check;
            //check if it is a number or not
            if (!double.TryParse(textBox_vol.Text, out check))
            {
                MessageBox.Show("Please enter a number >0");
                textBox_vol.Text = "Wrong";
            }
            //check if it is positive
            else if (Convert.ToDouble(textBox_vol.Text) < 0)
            {
                MessageBox.Show("Please enter a number >0");
                textBox_vol.Text = "Wrong";
            }
            else//if all true, flag goes to true
            {
                flag_vol = true;
            }
        }

        private void textBox_trials_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
