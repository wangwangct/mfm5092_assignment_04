﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Assignment_01
{
    //This class calculate all the outputs we want
    public class Without_CV
    {
        //instan. simulator calss
        Simulator sim = new Simulator();
        Stopwatch watch = new Stopwatch();

        public void scheduler()
        {
            //If users choose to use antithetic, then the value of IO.Antithetic is true, system generates antithetic prices, otherwise system prices option using the original way.
            if (IO.Antithetic == true)
            {
                watch.Start();
                //instan. a random matrix of the size
                double[,] r = new double[IO.Trials, IO.Steps];
                double[,] pricing_matrix = new double[1, (2 * IO.Trials) + 1];
                //compute and generate this random matrix
                r = sim.random();
                //As we need to calculate SE, option price of each simulation is needed, so we assign values to a matrix
                pricing_matrix = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);

                //calculate price, Greek values and SE
                IO.O_price = pricing_matrix[0, 0];
                Program.increase(1);
                IO.O_se = SE_ant(pricing_matrix);
                Program.increase(1);
                IO.O_delta = Delta_ant(r);
                Program.increase(1);
                IO.O_gamma = Gamma_ant(r);
                Program.increase(1);
                IO.O_vega = Vega_ant(r);
                Program.increase(1);
                IO.O_theta = Theta_ant(r);
                Program.increase(1);
                IO.O_rho = Rho_ant(r);
                Program.increase(1);
                watch.Stop();
                IO.Timer = watch.Elapsed.Hours.ToString() + ":"
                    + watch.Elapsed.Minutes.ToString() + ":"
                    + watch.Elapsed.Seconds.ToString() + ":"
                    + watch.Elapsed.Milliseconds.ToString();
                Program.finished();
            }
            else if (IO.Antithetic == false)
            {
                watch.Start();
                double[,] r = new double[IO.Trials, IO.Steps];
                double[,] pricing_matrix = new double[1, IO.Trials + 1];
                //compute and generate this random matrix
                r = sim.random();
                //As we need to calculate SE, option price of each simulation is needed, so we assign values to a matrix
                pricing_matrix = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);

                //calculate price, Greek values and SE
                IO.O_price = pricing_matrix[0, 0];
                Program.increase(1);
                IO.O_se = SE(pricing_matrix);
                Program.increase(1);
                IO.O_delta = Delta(r);
                Program.increase(1);
                IO.O_gamma = Gamma(r);
                Program.increase(1);
                IO.O_vega = Vega(r);
                Program.increase(1);
                IO.O_theta = Theta(r);
                Program.increase(1);
                IO.O_rho = Rho(r);
                Program.increase(1);
                watch.Stop();
                IO.Timer = watch.Elapsed.Hours.ToString() + ":"
                    + watch.Elapsed.Minutes.ToString() + ":"
                    + watch.Elapsed.Seconds.ToString() + ":"
                    + watch.Elapsed.Milliseconds.ToString();
                Program.finished();
            }

            //Call Garbage Collector to release RAM
            GC.Collect();
        }


        //==============================================================================================================================================
        //Pricing W/ Antithetic
        //==============================================================================================================================================
        //compute price, the output of this method is a 1X(1+IO.Trials) matrix. The 1st column is option price, the rest are prices of each simulation
        public double[,] Price_ant(double[,] r, int Trials, int Steps, double Tenor, double S, double K, double R, double vol)
        {
            if (IO.Call == true)//call option
            {
                double[,] price = new double[2 * Trials, Steps + 1];
                double dt = Tenor / Convert.ToDouble(Steps);

                for (int a = 0; a <= (2 * Trials) - 1; a++)
                {
                    price[a, 0] = S;
                }
                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                //for (int a = 0; a <= Trials - 1; a++)
                {
                    for (int b = 1; b <= Steps; b++)
                    {
                        price[a, b] = price[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                        price[(2 * Trials) - 1 - a, b] = price[(2 * Trials) - 1 - a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) - (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                });
                double sum = 0;
                double[,] Result = new double[1, (2 * Trials) + 1];
                for (int a = 0; a < (2 * Trials); a++)
                {
                    sum += Math.Max(price[a, Steps] - K, 0);

                }

                Result[0, 0] = (0.5 * sum * Math.Exp(-(R * Tenor))) / Convert.ToDouble(Trials);
                for (int a = 1; a <= (2 * Trials); a++)
                {
                    Result[0, a] = Math.Max(price[a - 1, Steps] - K, 0) * Math.Exp(-(R * Tenor));
                }

                return Result;
            }
            else//put option
            {
                double[,] price = new double[2 * Trials, Steps + 1];
                double dt = Tenor / Convert.ToDouble(Steps);

                for (int a = 0; a <= (2 * Trials) - 1; a++)
                {
                    price[a, 0] = S;
                }
                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                //for (int a = 0; a <= Trials - 1; a++)
                {
                    for (int b = 1; b <= Steps; b++)
                    {
                        price[a, b] = price[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                        price[(2 * Trials) - 1 - a, b] = price[(2 * Trials) - 1 - a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) - (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                });
                double sum = 0;
                double[,] Result = new double[1, (2 * Trials) + 1];
                for (int a = 0; a < (2 * Trials); a++)
                {
                    sum += Math.Max(K - price[a, Steps], 0);

                }

                Result[0, 0] = 0.5 * sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                for (int a = 1; a <= (2 * Trials); a++)
                {
                    Result[0, a] = Math.Max(K - price[a - 1, Steps], 0) * Math.Exp(-(R * Tenor));
                }

                return Result;
            }

        }

        //The following part calculates Greek values and SE if user choose to use antithetic
        //The pricing_matrix is used to save the output of Price method, and we just need the 1st lattice of each matrix to calculate Greek values.
        public double Delta_ant(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, (2 * IO.Trials) + 1];
            double[,] pricing_matrix_2 = new double[1, (2 * IO.Trials) + 1];
            pricing_matrix_1 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying + (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying - (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = (pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (0.002 * IO.Underlying);
            return result;
        }

        public double Gamma_ant(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, (2 * IO.Trials) + 1];
            double[,] pricing_matrix_2 = new double[1, (2 * IO.Trials) + 1];
            double[,] pricing_matrix_3 = new double[1, (2 * IO.Trials) + 1];
            pricing_matrix_1 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying + (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_3 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying - (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = (pricing_matrix_1[0, 0] - (2 * pricing_matrix_2[0, 0]) + pricing_matrix_3[0, 0]) / Math.Pow(0.001 * IO.Underlying, 2);
            return result;
        }

        public double Vega_ant(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, (2 * IO.Trials) + 1];
            double[,] pricing_matrix_2 = new double[1, (2 * IO.Trials) + 1];
            pricing_matrix_1 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility + (0.001 * IO.Volatility));
            pricing_matrix_2 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility - (0.001 * IO.Volatility));
            double result = (pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (0.002 * IO.Volatility);
            return result;
        }

        public double Theta_ant(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, (2 * IO.Trials) + 1];
            double[,] pricing_matrix_2 = new double[1, (2 * IO.Trials) + 1];
            pricing_matrix_1 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor + (IO.Tenor / Convert.ToDouble(IO.Steps)), IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = -(pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (IO.Tenor / Convert.ToDouble(IO.Steps));
            return result;
        }

        public double Rho_ant(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, (2 * IO.Trials) + 1];
            double[,] pricing_matrix_2 = new double[1, (2 * IO.Trials) + 1];
            pricing_matrix_1 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate + (0.001 * IO.Risk_free_rate), IO.Volatility);
            pricing_matrix_2 = Price_ant(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate - (0.001 * IO.Risk_free_rate), IO.Volatility);
            double result = (pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (0.002 * IO.Risk_free_rate);
            return result;
        }

        //Calculate standard error, the input of this method is already calculated when calculating option price.
        public double SE_ant(double[,] pricing_matrix)
        {
            double[,] ave = new double[1, IO.Trials];//generate an empty matrix to restore the average of antithetic prices
            double var = 0;
            for (int a = 0; a <= IO.Trials - 1; a++) //calculate the average of each pair of antithetic prices
            {
                ave[0, a] = (pricing_matrix[0, a + 1] + pricing_matrix[0, (2 * IO.Trials) - a]) / 2;
            }
            //for (int a = 0; a <= IO.Trials - 1; a++)//calculate mean
            //{
            //    mean += ave[0, a];
            //}
            //mean = mean / IO.Trials;
            for (int a = 0; a <= IO.Trials - 1; a++)//calculate variance
            {
                var += Math.Pow(ave[0, a] - pricing_matrix[0, 0], 2);
            }
            var = var / (IO.Trials - 1);
            //calculate standard error 
            double result = Math.Sqrt(var / IO.Trials);
            return result;
        }
        //==============================================================================================================================================
        //Pricing W/ Antithetic
        //==============================================================================================================================================




        //==============================================================================================================================================
        //Pricing W/O Antithetic
        //==============================================================================================================================================
        public double[,] Price(double[,] r, int Trials, int Steps, double Tenor, double S, double K, double R, double vol)
        {
            if (IO.Call == true)//call option
            {
                double[,] price = new double[Trials, Steps + 1];
                double dt = Tenor / Convert.ToDouble(Steps);

                for (int a = 0; a <= Trials - 1; a++)
                {
                    price[a, 0] = S;
                }
                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                //for (int a = 0; a <= Trials - 1; a++)
                {
                    for (int b = 1; b <= Steps; b++)
                    {
                        price[a, b] = price[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                });
                double sum = 0;
                double[,] Result = new double[1, Trials + 1];
                for (int a = 0; a < Trials; a++)
                {
                    sum += Math.Max(price[a, Steps] - K, 0);

                }

                Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                for (int a = 1; a <= Trials; a++)
                {
                    Result[0, a] = Math.Max(price[a - 1, Steps] - K, 0) * Math.Exp(-(R * Tenor));
                }

                return Result;
            }
            else//put option
            {
                double[,] price = new double[Trials, Steps + 1];
                double dt = Tenor / Convert.ToDouble(Steps);

                for (int a = 0; a <= Trials - 1; a++)
                {
                    price[a, 0] = S;
                }
                Parallel.ForEach(Ienum.Step(0, Trials - 1, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, a =>
                //for (int a = 0; a <= Trials - 1; a++)
                {
                    for (int b = 1; b <= Steps; b++)
                    {
                        price[a, b] = price[a, b - 1] * Math.Exp(((R - (Math.Pow(vol, 2) / 2)) * dt) + (vol * Math.Sqrt(dt) * r[a, b - 1]));
                    }
                });
                double sum = 0;
                double[,] Result = new double[1, Trials + 1];
                for (int a = 0; a < Trials; a++)
                {
                    sum += Math.Max(K - price[a, Steps], 0);

                }

                Result[0, 0] = sum * Math.Exp(-(R * Tenor)) / Convert.ToDouble(Trials);
                for (int a = 1; a <= Trials; a++)
                {
                    Result[0, a] = Math.Max(K - price[a - 1, Steps], 0) * Math.Exp(-(R * Tenor));
                }

                return Result;
            }

        }

        public double Delta(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_2 = new double[1, IO.Trials + 1];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying + (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying - (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = (pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (0.002 * IO.Underlying);
            return result;
        }

        public double Gamma(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_2 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_3 = new double[1, IO.Trials + 1];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying + (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_3 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying - (0.001 * IO.Underlying), IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = (pricing_matrix_1[0, 0] - (2 * pricing_matrix_2[0, 0]) + pricing_matrix_3[0, 0]) / Math.Pow(0.001 * IO.Underlying, 2);
            return result;
        }

        public double Vega(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_2 = new double[1, IO.Trials + 1];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility + (0.001 * IO.Volatility));
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility - (0.001 * IO.Volatility));
            double result = (pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (0.002 * IO.Volatility);
            return result;
        }

        public double Theta(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_2 = new double[1, IO.Trials + 1];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor + (IO.Tenor / Convert.ToDouble(IO.Steps)), IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate, IO.Volatility);
            double result = -(pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (IO.Tenor / Convert.ToDouble(IO.Steps));
            return result;
        }

        public double Rho(double[,] r)
        {
            double[,] pricing_matrix_1 = new double[1, IO.Trials + 1];
            double[,] pricing_matrix_2 = new double[1, IO.Trials + 1];
            pricing_matrix_1 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate + (0.001 * IO.Risk_free_rate), IO.Volatility);
            pricing_matrix_2 = Price(r, IO.Trials, IO.Steps, IO.Tenor, IO.Underlying, IO.Strike_Price, IO.Risk_free_rate - (0.001 * IO.Risk_free_rate), IO.Volatility);
            double result = (pricing_matrix_1[0, 0] - pricing_matrix_2[0, 0]) / (0.002 * IO.Risk_free_rate);
            return result;
        }

        //Calculate standard error, the input of this method is already calculated when calculating option price.
        public double SE(double[,] pricing_matrix)
        {
            double sum = 0;
            for (int a = 1; a <= IO.Trials; a++)
            {
                sum += Math.Pow(pricing_matrix[0, a] - pricing_matrix[0, 0], 2);
            }
            double SD = Math.Sqrt((sum) / (IO.Trials - 1));
            double result = SD / Math.Sqrt(IO.Trials);
            return result;
        }

        //==============================================================================================================================================
        //Pricing W/O Antithetic
        //==============================================================================================================================================
    }
}
